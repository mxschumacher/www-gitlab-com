---
layout: handbook-page-toc
title: "Recruiting Process - Recruiting Operations Tasks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruiting Process Framework - Recruiting Operations Tasks

### System Access

To gain access to a system listed below, please follow the respective link to the appropriate page on how to go about submitting an Access Request Issue:

#### **ContactOut**
* Sourcing Team only
#### DocuSign
* C.E.S. and Sales Operations Team 
#### Greenhouse
[See how to join Greenhouse in the handbook.](https://about.gitlab.com/handbook/hiring/greenhouse/#how-to-join-greenhouse)
#### LinkedIn Recruiter
[See the LinkedIn Recruiter Seat Request process in the handbook.](https://about.gitlab.com/handbook/hiring/sourcing/#upgrading-your-linkedin-account)

### System Processes

#### **DocuSign**
* TBA
#### **Greenhouse**
* **Candidate Profile Merge Requests**
   * Consider the following when merging candidate/prospect profiles:
      * Ensure that the Source, Coordinator and Recruiter listed in the Details tab remains the same.
      * If a candidate is marked as *Hired*, ensure the profile that they were *Hired* on is the Primary Profile.

* **Offer and Requisition Approvals**

* **Referral Submissions**

[See how to process Referral Submissions in the handbook.](https://about.gitlab.com/handbook/hiring/referral-operations/#transferring-referral-submissions-to-greenhouse)

### Reporting

* **Monthly Metrics Reports**
* **Weekly, Monthly, and Quarterly Reports**
* **Ad Hoc Reports**
    * [Report Request](https://gitlab.com/gl-recruiting/operations/-/issues/new?issuable=Report%20Request)

### Common Issues
The Recruiting Operations & Insights team utilizes [Recruiting Operations Issues](https://gitlab.com/gl-recruiting/operations/-/issues) to provide support for a variety of requests regarding systems support and access requests across Recruiting's systems.

* **LinkedIn Recruiter**
* **Greenhouse Offer and Requisition Approvals**
* **System Integrations**
* **Re-Opening Requisitions**
