---
layout: handbook-page-toc
title: "Professional Services (PS) Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Professional Services (PS) Finance

## Professional Services at GitLab

The Professional Services (PS) at GitLab forms a part for the Customer Success department. Professional Services is classified into 3 verticals - Consulting Delivery, Education Delivery, and Practice Management. As Consulting Delivery and Education Delivery form the Client serving teams they are expenses as Cost of Goods Sold (CoGS), while Practice Management which deals with internal management and support to the Client Serving verticals is expensed as Operating Expense (OpEx).

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcbiAgQVtQcm9mZXNzaW9uYWwgU2VydmljZXNdXG4gIEEgLS0-fENvR1N8IEJbQ29uc3VsdGluZyBEZWxpdmVyeV1cbiAgQSAtLT58Q29HU3wgQ1tFZHVjYXRpb24gRGVsaXZlcnldXG4gIEEgLS0-fE9wRXh8IERbUHJhY3RpY2UgTWFuYWdlbWVudF1cblx0XHQiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbiAgQVtQcm9mZXNzaW9uYWwgU2VydmljZXNdXG4gIEEgLS0-fENvR1N8IEJbQ29uc3VsdGluZyBEZWxpdmVyeV1cbiAgQSAtLT58Q29HU3wgQ1tFZHVjYXRpb24gRGVsaXZlcnldXG4gIEEgLS0-fE9wRXh8IERbUHJhY3RpY2UgTWFuYWdlbWVudF1cblx0XHQiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)

Consulting Delivery deals with technology services like implementation, migration, etc., while Education Delivery provides training and certification services.


# Professional Services Offerings:


- Implementation Services
- Migration Services
- Education Services
- Integration Services
- Innersourcing Services

More details of the offerings can be found at the PS Offering [Page](https://about.gitlab.com/services/>) and full [cataog](https://about.gitlab.com/services/catalog/)

# Selling Professional Services:

Professional services are usually sold through 2 packages:

Off-the-Shelf SKU’s [Standard SKU’s](ttps://about.gitlab.com/handbook/customer-success/professional-services-engineering/SKUs/) and Custom SKU’s.

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcbiAgQVtTdGFuZGFyZCBTZXJ2aWNlXVxuICBBIC0tPnxZZXN8IEJbT2ZmLXRoZS1zaGVsZiBTS1Unc11cbiAgQSAtLT58Tm98IENbQ3VzdG9tZSBTS1UsIGNvbnRhY3QgUFMgVGVhbV1cbiAgXG5cdFx0IiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbiAgQVtTdGFuZGFyZCBTZXJ2aWNlXVxuICBBIC0tPnxZZXN8IEJbT2ZmLXRoZS1zaGVsZiBTS1Unc11cbiAgQSAtLT58Tm98IENbQ3VzdG9tZSBTS1UsIGNvbnRhY3QgUFMgVGVhbV1cbiAgXG5cdFx0IiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)

A detailed process on the selling of Professional Services can be found [here](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/working-with/)